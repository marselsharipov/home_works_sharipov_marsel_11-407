import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

@WebServlet(name = "MyServlet")
public class MyServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession httpSession = request.getSession();
        request.setCharacterEncoding("UTF-8");

        String fio = request.getParameter("fio");
        String pnumber = request.getParameter("pnumber");
        String email = request.getParameter("email");
        String bday = request.getParameter("bday");
        String job = request.getParameter("job");
        String comment = request.getParameter("comment");


        if (request.getSession().getAttribute("list") == null) {
            ArrayList<People> list = new ArrayList<People>();
            list.add(new People(fio, pnumber, email, bday, job, comment));
            Collections.sort(list, new Comparator<People>() {
                public int compare(People h1, People h2) {
                    return h1.getmFio().compareTo(h2.getmFio());
                }
            });
            httpSession.setAttribute("list", list);
        } else {
            ArrayList<People> list = (ArrayList<People>) request.getSession().getAttribute("list");
            list.add(new People(fio, pnumber, email, bday, job, comment));
            Collections.sort(list, new Comparator<People>() {
                public int compare(People h1, People h2) {
                    return h1.getmFio().compareTo(h2.getmFio());
                }
            });
            httpSession.setAttribute("list", list);
        }
        response.sendRedirect("/my_servlet-build-572");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html");

        ArrayList<People> list = (ArrayList<People>) request.getSession().getAttribute("list");

        if ((request.getParameter("fio") != null) && (list != null)) {
            for (People h : list) {
                if (h.getmFio().equals(request.getParameter("fio"))) {
                    response.getWriter().print("Name : " + h.getmFio() + "<br>");
                    response.getWriter().print("Job : " + h.getmJob() + "<br>");
                    response.getWriter().print("Phone number : " + h.getPhNumber() + "<br>");
                    response.getWriter().print("Birthday : " + h.getbDay() + "<br>");
                    response.getWriter().print("email : " + h.geteMail() + "<br>");
                    response.getWriter().print("Comment : " + h.getmComment() + "<br>");
                    response.getWriter().print("<br>");
                    response.getWriter().print("<a href=\"/my_servlet-build-572\">BACK<a/>");

                }
            }
        } else {
            response.getWriter().println("<form action=\"/my_servlet-build-572\" method=\"POST\"><br>");
            response.getWriter().println("<input type=\"text\" size=\"40\" name=\"fio\" placeholder=\"Name\"/><br>");
            response.getWriter().println("<input type=\"text\" size=\"40\" name=\"pnumber\" placeholder=\"Phone Number\"/><br>");
            response.getWriter().println("<input type=\"text\" size=\"40\" name=\"email\" placeholder=\"E-mail\"/><br>");
            response.getWriter().println("<input type=\"text\" size=\"40\" name=\"bday\" placeholder=\"Birthday\"/><br>");
            response.getWriter().println("<input type=\"text\" size=\"40\" name=\"job\" placeholder=\"Job\"/><br>");
            response.getWriter().println("<input type=\"text\" size=\"100\" name=\"comment\" placeholder=\"Comment\"/><br>");
            response.getWriter().println("<input type=\"submit\" value=\"Add\"/>");
            response.getWriter().print("<br>");
            response.getWriter().print("<br>");

            if (list != null) {
                for (People h : list) {
                    response.getWriter().print(h + "<br>");
                }
            }
        }
    }
}

