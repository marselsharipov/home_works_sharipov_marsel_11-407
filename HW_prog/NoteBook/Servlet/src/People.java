public class People implements Comparable<People> {
    private String mFio, eMail, bDay, phNumber, mComment, mJob;
    private int id=1;

    public People(String fio, String pnumber, String email, String bday, String job, String comment){
        this.mFio =fio;
        this.eMail =email;
        this.bDay =bday;
        this.phNumber =pnumber;
        this.mJob =job;
        this.mComment =comment;
    }


    public String toString() {
        return "<a href=\"/my_servlet-build-572?fio=" + this.mFio + "\">" + this.mFio + "<a/> " + "\t" + this.phNumber + "\t" + this.geteMail();
    }

    public String getmFio() {
        return mFio;
    }
    public String geteMail(){
        return eMail;
    }

    public String getPhNumber() {
        return phNumber;
    }

    public String getmJob() {
        return mJob;
    }

    public String getmComment() {
        return mComment;
    }

    public String getbDay() {
        return bDay;
    }

    @Override
    public int compareTo(People h) {
        return this.mFio.compareToIgnoreCase(h.getmFio());
    }

}
